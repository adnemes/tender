import requests
import math
import traceback
import time
import re
from xml.dom.minidom import parseString
from openpyxl import load_workbook, Workbook
from openpyxl.styles import PatternFill
from geopy.geocoders import Nominatim
geolocator = Nominatim()

routes_xls_file_path = 'route4.xlsx'
destination_file_path = 'sasol_done.xlsx'

'''
http://www.zippopotam.us/#where
{j[1]:(j[0], tuple(n.strip() for n in j[3].split(':'))) for j in [i.split('\t') for i in s.split('\n')] if len(j)>3}
'''

country_codes = {'AD': ('Andorra', ('AD100', 'AD700')), 'AR': ('Argentina', ('1601', '9431')), 'AS': ('American Samoa', ('96799', '96799')), 'AT': ('Austria', ('1010', '9992')), 'AU': ('Australia', ('0200', '9726')), 'BD': ('Bangladesh', ('1000', '9461')), 'BE': ('Belgium', ('1000', '9992')), 'BG': ('Bulgaria', ('1000', '9974')), 'BR': ('Brazil', ('01000-000', '99990-000')), 'CA': ('Canada', ('A0A', 'Y1A')), 'CH': ('Switzerland', ('1000', '9658')), 'CZ': ('Czech Republic', ('10000', '79862')), 'DE': ('Germany', ('01067', '99998')), 'DK': ('Denmark', ('0800', '9990')), 'DO': ('Dominican Republic', ('10101', '11906')), 'ES': ('Spain', ('01001', '52080')), 'FI': ('Finland', ('00002', '99999')), 'FO': ('Faroe Islands', ('100', '970')), 'FR': ('France', ('01000', '98799')), 'GB': ('Great Britain', ('AB1', 'ZE3')), 'GF': ('French Guyana', ('97300', '97390')), 'GG': ('Guernsey', ('GY1', 'GY9')), 'GL': ('Greenland', ('2412', '3992')), 'GP': ('Guadeloupe', ('97100', '97190')), 'GT': ('Guatemala', ('01001', '22027')), 'GU': ('Guam', ('96910', '96932')), 'GY': ('Guyana', ('97312', '97360')), 'HR': ('Croatia', ('10000', '53296')), 'HU': ('Hungary', ('1011', '9985')), 'IM': ('Isle of Man', ('IM1', 'IM9')), 'IN': ('India', ('110001', '855126')), 'IS': ('Iceland', ('101', '902')), 'IT': ('Italy', ('00010', '98168')), 'JE': ('Jersey', ('JE1', 'JE3')), 'JP': ('Japan', ('100-0001', '999-8531')), 'LI': ('Liechtenstein', ('9485', '9498')), 'LK': ('Sri Lanka', ('*', '96167')), 'LT': ('Lithuania', ('00001', '99069')), 'LU': ('Luxembourg', ('L-1009', 'L-9999')), 'MC': ('Monaco', ('98000', '98000')), 'MD': ('Moldavia', ('MD-2000', 'MD-7731')), 'MH': ('Marshall Islands', ('96960', '96970')), 'MK': ('Macedonia', ('1000', '7550')), 'MP': ('Northern Mariana Islands', ('96950', '96952')), 'MQ': ('Martinique', ('97200', '97290')), 'MX': ('Mexico', ('01000', '99998')), 'MY': ('Malaysia', ('01000', '98859')), 'NL': ('Holland', ('1000', '9999')), 'NO': ('Norway', ('0001', '9991')), 'NZ': ('New Zealand', ('0110', '9893')), 'PH': ('Phillippines', ('0400', '9811')), 'PK': ('Pakistan', ('10010', '97320')), 'PL': ('Poland', ('00001', '99440')), 'PM': ('Saint Pierre and Miquelon', ('97500', '97500')), 'PR': ('Puerto Rico', ('00601', '00988')), 'PT': ('Portugal', ('1000001', '9980999')), 'RE': ('French Reunion', ('97400', '97490')), 'RU': ('Russia', ('101000', '901993')), 'SE': ('Sweden', ('10005', '98499')), 'SI': ('Slovenia', ('1000', '9600')), 'SJ': ('Svalbard & Jan Mayen Islands', ('8099', '9178')), 'SK': ('Slovak Republic', ('01001', '99201')), 'SM': ('San Marino', ('47890', '47899')), 'TH': ('Thailand', ('10100', '96220')), 'TR': ('Turkey', ('01000', '81950')), 'US': ('United States', ('00210', '99950')), 'VA': ('Vatican', ('00120', '00120')), 'VI': ('Virgin Islands', ('00801', '00851')), 'YT': ('Mayotte', ('97600', '97680')), 'ZA': ('South Africa', ('0002', '9992'))}

processed_waypoints = {}

def load_routes(filename):
    '''
    -> (Loading country, Loading ZIP, Loading City, Unloading country, Unloading ZIP, Unloading City)
    '''
    wb = load_workbook(filename = filename)
    sheet = wb.active
    row = 2
    while sheet['A{}'.format(row)].value != None and \
          sheet['B{}'.format(row)].value != None and \
          sheet['D{}'.format(row)].value != None and \
          sheet['E{}'.format(row)].value != None:
        yield sheet['A{}'.format(row)].value, \
              sheet['B{}'.format(row)].value, \
              sheet['C{}'.format(row)].value, \
              sheet['D{}'.format(row)].value, \
              sheet['E{}'.format(row)].value, \
              sheet['F{}'.format(row)].value
        row +=1

def write_out_routes(processed_routes):
    wb = Workbook()
    ws1 = wb.active
    set_header_row(ws1)
    for row, ((loading_country, loading_zip, loading_city, unloading_country, unloading_zip, unloading_city), destination, from_address, to_address) in enumerate(processed_routes, 2):
        print(row - 1, loading_country, loading_zip, loading_city, unloading_country, unloading_zip, unloading_city, destination)
        ws1['A{}'.format(row)] = loading_country
        ws1['B{}'.format(row)] = loading_zip
        ws1['C{}'.format(row)] = loading_city
        ws1['D{}'.format(row)] = unloading_country
        ws1['E{}'.format(row)] = unloading_zip
        ws1['F{}'.format(row)] = unloading_city
        ws1['G{}'.format(row)] = destination
        ws1['H{}'.format(row)] = from_address
        ws1['I{}'.format(row)] = to_address
        color_non_zip(ws1, 'H{}'.format(row), from_address, loading_zip)
        color_non_zip(ws1, 'I{}'.format(row), to_address, unloading_zip)
    wb.save(filename = destination_file_path)

def set_header_row(ws1):
    ws1['A1'] = 'Loading country'
    ws1['B1'] = 'Loading ZIP'
    ws1['C1'] = 'Loading City'
    ws1['D1'] = 'Unloading country'
    ws1['E1'] = 'Unloading ZIP'
    ws1['F1'] = 'Unloading City'

def color_non_zip(sheet, cell_row, address, zip_code):
    if not any(i.startswith(str(zip_code)) for i in ''.join(address.split(',')).split(' ')):
        redFill = PatternFill(start_color='FFFF0000', end_color='FFFF0000', fill_type='solid')
        sheet[cell_row].fill = redFill

def print_out_routes(processed_routes):
    for row, ((from_loc, to_loc), destination, from_address, to_address) in enumerate(processed_routes, 1):
        print(row, from_loc, to_loc, destination, from_address, to_address)

def get_waypoints_by_google(country_code, zip, city):
    try:
        cordinate_request_link = 'https://maps.googleapis.com/maps/api/geocode/xml?address={}+{}+{}&key=AIzaSyB1sYuzm6S6AGqyXcHe_5r5sElt9n3PQVg'.format(country_codes[country_code][0], '' if city else zip, city)
        response = requests.get(cordinate_request_link)
        parsed_response = parseString(response.text)
        location = parsed_response.getElementsByTagName('location')
        waypoints = [y.firstChild.data for y in [x for x in location][0].childNodes if y.nodeType == y.ELEMENT_NODE]
        address = parsed_response.getElementsByTagName('formatted_address')[0].firstChild.data
        return waypoints, address
    except:
        print('error occurred on get_waypoints_by_google!: %s %s' % (country_codes[country_code][0], city))
        traceback.print_exc()

def get_waypoints_by_geolocatior(country_code, zip, city):
    try:
        location = geolocator.geocode('%s %s' % (country_codes[country_code][0], city))
        # if location and str(zip) not in location.address:
            # print('look out zip code cannot be found in the found address! %s %s %s'% (country_code, zip, city))
        if location:
            return (location.latitude, location.longitude), location.address
        else:
            raise Exception('geolocatior cannot find anything for %s %s' % (country_codes[country_code][0], city))
    except:
        print('error occurred on get_waypoints_by_geolocatior!: %s %s' % (country_codes[country_code][0], city))
        traceback.print_exc()
        return get_waypoints_by_google(country_code, zip, city)

def get_waypoints_by_geonames(country_code, zip, city):
    try:
        link = 'http://www.geonames.org/postalcode-search.html?q=%s&country=%s' % (city, country_code)
        print('debug: %s' % link)
        findings = _get_waypoints_by_geonames_get_findings(country_code, zip, city)
        # response = requests.get('http://www.geonames.org/postalcode-search.html?q=%s&country=%s' % (city, country_code))
        # findings = re.finditer('<small>\d+<\/small></td><td>[^<]+</td><td>([^<]+)<\/td>(<td>.+<\/td>)+', response.text)
        possible_return = None
        for i, matched in enumerate(findings):
            full_zip = matched.group(1)
            print('debug full_zip: %s' % full_zip)
            matched_one = matched.group(2)
            print('debug matched_one: %s' % matched_one)
            waypoint_match = re.search('<small>([\d.-]+)\/([\d.-]+)</small>', matched_one)
            waypoints = waypoint_match.group(1), waypoint_match.group(2)
            address = matched_one.replace('<td>', '').replace('</td>', ' ')
            address = address[:address.index('<')]
            if i == 0:
                possible_return = waypoints, address
            if str(zip) in full_zip:
                possible_return = waypoints, address
            if full_zip.startswith(str(zip)):
                address += ' ' + full_zip
                return waypoints, address
        else:
            if possible_return:
                return possible_return
            else:
                raise Exception('nothing was found')
    except:
        print('error occurred on get_waypoints_by_geonames!: %s %s' % (country_codes[country_code][0], city))
        traceback.print_exc()
        return get_waypoints_by_geolocatior(country_code, zip, city)

def get_geoname_response(zip_city, country_code):
    response = requests.get('http://www.geonames.org/postalcode-search.html?q=%s&country=%s' % (zip_city, country_code))
    return  re.finditer('<small>\d+<\/small></td><td>[^<]+</td><td>([^<]+)<\/td>(<td>.+<\/td>)+', response.text)

def _get_waypoints_by_geonames_get_findings(country_code, zip, city):
    if city:
        return get_geoname_response(city, country_code)
    else:
        first_zip_code = country_codes[country_code][1][0]
        print('first_zip_code: %s' % first_zip_code)
        new_full_zip = str(zip).rjust(2, '0') + (len(first_zip_code) - 2) * '0'
        print('new_full_zip: %s' % new_full_zip)
        return _get_waypoints_by_geonames_get_findings_by_zip_code_guess(new_full_zip, country_code)

def _get_waypoints_by_geonames_get_findings_by_zip_code_guess(zip, country_code):
    orig_len = len(str(zip))
    findings = list(get_geoname_response(zip, country_code))
    count = 0
    while not findings:
        zip = zip[:2] + str(int(zip[2:]) + 1).rjust(orig_len - 2, '0')
        print('newzip: ', zip)
        if orig_len < len(zip) or (country_code in ('PT', 'LU') and count > 200): #'LU' 04, 05 EXIst but nothing on 40 or 50
            break
        findings = list(get_geoname_response(zip, country_code))
        count += 1
        print('findings: ', findings)
    print('got findings with %s' % zip)
    return findings

def get_waypoints(country_code, zip, city):
    if (country_code, zip, city) in processed_waypoints:
        return processed_waypoints[(country_code, zip, city)]
    else:
        waypoints, address = get_waypoints_by_geonames(country_code, zip, city)
        processed_waypoints[(country_code, zip, city)] = waypoints, address
        return waypoints, address

def get_distance(latitude1, longitude1, latitude2, longitude2):
    '''
    -> 1419 km
    '''
    try:
        route_request_link = 'https://route.cit.api.here.com/routing/7.2/calculateroute.json?waypoint0={}%2C{}&waypoint1={}%2C{}&mode=fastest%3Btruck%3Btraffic%3Adisabled&limitedWeight=30&height=4&app_id=DemoAppId01082013GAL&app_code=AJKnXv84fjrb0KIHawS0Tg&departure=now'.format(latitude1, longitude1, latitude2, longitude2)
        #print(latitude1, longitude1, latitude2, longitude2)
        print('route_request_link: ',route_request_link)
        request_result = requests.get(route_request_link).json()
        if 'response' in request_result:
            route_distance = request_result['response']['route'][-1]['summary']['distance']
        else:
            raise Exception('No route has been found\nroute request: %s\nrequest result: %s' % (route_request_link, request_result))
        # return '{} km'.format(route_distance)
        distance = int(math.ceil(route_distance / 1000))
        return '{}'.format(distance + 10 - int(str(distance)[-1]))
    except Exception as error:
        print('error occurred on get_distance!\n%s' % error)
        traceback.print_exc()
        return 'No route found'

def reform_city(city):
    if city and '/' in city:
        return city[:city.index('/')]
    else:
        return city

def reform_zip(zip):
    if not zip:
        return '00'
    elif len(str(zip)) >= 2:
        return zip
    else:
        return '0' + str(zip)


def establish_routes(routes):
    '''
    -> (('Budapest', 'Lyon'), 1419 km)
    '''
    for loading_country, loading_zip, loading_city, unloading_country, unloading_zip, unloading_city in routes:
        # print('establish route: %s - %s' % (from_loc, to_loc))
        try:
            loading_city = reform_city(loading_city)
            unloading_city = reform_city(unloading_city)
            loading_zip = reform_zip(loading_zip)
            unloading_zip = reform_zip(unloading_zip)
            from_loc_waypoints, from_address = get_waypoints(loading_country, loading_zip, loading_city)
            to_loc_waypoints, to_address = get_waypoints(unloading_country, unloading_zip, unloading_city)
            distance = get_distance(*from_loc_waypoints, *to_loc_waypoints) if from_loc_waypoints and to_loc_waypoints else 'No data'
            yield (loading_country, loading_zip, loading_city, unloading_country, unloading_zip, unloading_city), distance, from_address, to_address
        except Exception as error:
            print('error occurred on establish_routes!\n%s' % error)
            traceback.print_exc()
            yield (loading_country, loading_zip, loading_city, unloading_country, unloading_zip, unloading_city), 'No data', '', ''



if __name__ == '__main__':
    routes_list = load_routes(routes_xls_file_path)
    print('routes have been loaded sucessfully!')
    processed_routes = establish_routes(routes_list)
    print('routes are being established!')
    write_out_routes(processed_routes)
    print('routes have been written out into {} file sucessfully!'.format(destination_file_path))
    # print_out_routes(processed_routes)
